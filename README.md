# README #

This is a JS/Canvas app to demonstrate a simple implementation of a virtual measuring instrument.

[Online demo](http://darrenspriet.com/experiments/RulerDemo/RulerDemo.html)

### Improvements that could be made (aside from the obvious lack of style) ###

* Currently there is no guard agaisnt using multiple touches. Using multiple touches with cause bugs.
* Add mouse event handling
* Add line updating/deleting
* Better overlap avoidance by the length texts
* Offset the ending anchor point away from under the finger, so that you can see it

### Other implementation ideas ###

* Virtual ruler instead of a generic measuring instrument
* Users could read the ruler like a real ruler, relying on labels and notches on the ruler
* Users could rotate the ruler similar to how it would be done in real life, flat on a surface