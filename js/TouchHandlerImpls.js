var LayerTouchHandlerDefault = {
	touchstart: function(evt, touch) {
	  var touchPos = {
	    x: touch.hitX,
	    y: touch.hitY
	  };
	  var shape = this.getIntersection(touchPos);
	  if(shape) {
	    touch.shape = shape;
	    if(shape.down && shape.down(touch));
	  }
	  touch.notifyDown();
	},
	touchend: function(evt, touch) {
	  var touchPos = {
	    x: touch.hitX,
	    y: touch.hitY
	  };
	  //log("hit: " + touch.hitX + " " + touch.hitY + " stage: " + touch.stageX + " " + touch.stageY);
	  var shape = this.getIntersection(touchPos);

	  if(shape && shape == touch.shape) {
	    if(touch.shape.up && touch.shape.up(touch));
	    touch.shape = null;
	  }
	  //log("dispatching notify up");
	  touch.notifyUp();
	},
	touchmove: function(evt, touch) {
	    var touchPos = {
	      x: touch.hitX,
	      y: touch.hitY
	    };
	    var shape = this.getIntersection(touchPos);
	    if(shape) {
	      if(touch.shape == null) {
	        //log("entered a shape");
	        touch.shape = shape;
	        if(shape.enter && shape.enter(touch));
	        if(shape.move && shape.move(touch));
	        //log("handled enter");
	      } else if(touch.shape != shape) {
	        if(touch.shape.exit && touch.shape.exit(touch));
	        touch.shape = shape;
	        if(shape.enter && shape.enter(touch));
	        if(shape.move && shape.move(touch));
	        //log("handled shape swap");
	      } else {
	        if(shape.move && shape.move(touch));
	      }
	    } else { //no shape intersection
	      if(touch.shape !== null) {
	        if(touch.shape.exit && touch.shape.exit(touch));
	        touch.shape = null;
	        //log("handled exit");
	      }
	    }
	    touch.notifyMove();
	},
	touchcancel: function(evt, touch) {
	    var touchPos = {
	      x: touch.hitX,
	      y: touch.hitY
	    };
	    var shape = this.getIntersection(touchPos);
	    if(shape) {
	      if(touch.shape.up && touch.shape.up(touch));
	      touch.shape = undefined;
	    }
	    touch.notifyUp();
	} 
};

//Sample
var EmptyLayerTouchHandler = {
	touchstart: function(evt, touch) {
	},
	touchend: function(evt, touch) {
	},
	touchmove: function(evt, touch) {
	},
	touchcancel: function(evt, touch) {
	} 
};

//Sample
var EmptyElementTouchHandler = {
	up: function(touch, isNotification) {
	},
	down: function(touch, isNotification) {
	},
	enter: function(touch, isNotification) {
	},
	exit: function(touch, isNotification) { 
	},
	move: function(touch, isNotification) { 
	}
};