var RulerSurface = function(layer) {

	this.surfaceBackground = new Kinetic.Rect({
		fill: '#FFE9BF', //grey
		width: AutoStage.sizeInfo().stageWidth,
		height: AutoStage.sizeInfo().stageHeight
	});

	var segmentMarkerConfig = {
		radius: 2,
		fill: 'red',
		x: 10, y: 10,
		listening: false
	};

	var SurfaceState = {
		isDrawingSegment: false,
		currentStartAnchor: null,
		lastEndAnchor: null,
		currentLineSegment: null,
		segmentMarker: new Kinetic.Circle(segmentMarkerConfig)
	}

	var SurfaceTouchHandler = {
		up: function(touch, isNotification) {
			if(this.isDrawingSegment) {
				this.isDrawingSegment = false;
				var pos = {x: touch.stageX, y: touch.stageY};
				var distance = Math.pow((pos.x - this.currentStartAnchor.circle.x()), 2) + Math.pow((pos.y - this.currentStartAnchor.circle.y()), 2)
				distance = Math.pow(distance, 0.5);
				if(distance > 50) {
					var anchor = new SegmentAnchor(layer);
					anchor.circle.x(pos.x);
					anchor.circle.y(pos.y);
					this.lastEndAnchor = anchor;
				} else {
					this.currentLineSegment.destroy();
					this.currentStartAnchor.circle.destroy();
				}
				this.currentStartAnchor = null;
				this.currentLineSegment = null;
				this.segmentMarker.destroy();
				layer.batchDraw();
			}
		},
		down: function(touch, isNotification) {
			this.isDrawingSegment = true;
			var pos = {x: touch.stageX, y: touch.stageY};
			var anchor = new SegmentAnchor(layer);
			anchor.circle.x(pos.x);
			anchor.circle.y(pos.y);
			this.currentStartAnchor = anchor;
			var lineSeg = new SegmentLine(layer);
			this.currentLineSegment = lineSeg;
			layer.batchDraw();
		},
		enter: function(touch, isNotification) {
		},
		exit: function(touch, isNotification) { 
		},
		move: function(touch, isNotification) { 
			if(this.isDrawingSegment) {
				var pos = {x: touch.stageX, y: touch.stageY};
				layer.add(this.segmentMarker);
				this.segmentMarker.x(pos.x);
				this.segmentMarker.y(pos.y);
				var linePoints = [this.currentStartAnchor.circle.x(), this.currentStartAnchor.circle.y(), this.segmentMarker.x(), this.segmentMarker.y()];
				this.currentLineSegment.setLinePoints(linePoints);
				layer.batchDraw();
			}
		}
	};

	stampit.mixIn(this.surfaceBackground, SurfaceState, SurfaceTouchHandler);
	layer.add(this.surfaceBackground);

};