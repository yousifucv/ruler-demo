
var SegmentAnchor = function(layer) {

	this.circle = null;
	this.circleHighlight = null;
	this.isSelected = null;

	var circleConfig = {
		x: 100, y: 100,
		radius: 3,
		fill: 'black',
		listening:false
	};
	
	var circleHighlightConfig = {
		x: 100, y: 100,
		radius: 7,
		fill: 'white',
		listening:false
	};

	this.circle = new Kinetic.Circle(circleConfig);
	var ElementTouchHandler = {
		up: function(touch, isNotification) {
		},
		down: function(touch, isNotification) {
		},
		enter: function(touch, isNotification) {
		},
		exit: function(touch, isNotification) { 
		},
		move: function(touch, isNotification) { 
		}
	};

	stampit.mixIn(this.circle, ElementTouchHandler);

	layer.add(this.circle);
};