var SegmentLine = function(layer) {
	var lineHit;
	var centrePoint = {x:-10,y:-10};
	var rotation = 0;
	var length;
	var lengthText;

	var p1 = {x:0,y:0};
	var p2 = {x:0,y:0};

	var lineConfig = {
		points: [p1.x,p1.y,p2.x,p2.y],
		stroke: 'black',
		listening: false
	};
	this.line = new Kinetic.Line(lineConfig);
	this.centreMarker = new Kinetic.Circle({
		x: centrePoint.x, y: centrePoint.y,
		radius: 2,
		fill: 'yellow',
		listening: false
	});
	this.lengthText = new Kinetic.Text({
		text: '',
		fill: 'black',
		x: -100, y: -100,
		padding: 5,
		offsetX: 30,
		rotation: 15,
		shadowColor: 'white',
		shadowOpacity: 1,
		shadowBlur: 30,
		listening: false
	});
	this.setLinePoints = function(points) {
		this.line.points(points);
		var p1 = {x:points[0], y: points[1]};
		var p2 = {x:points[2], y: points[3]};
		this.centrePoint = {x: Math.abs(p1.x + p2.x) / 2, y: Math.abs(p1.y + p2.y) / 2};
		this.centreMarker.x(this.centrePoint.x);
		this.centreMarker.y(this.centrePoint.y);
		this.lengthText.x(this.centrePoint.x);
		this.lengthText.y(this.centrePoint.y);
		//calculate rotation
		if(p2.x + 0.001 > p1.x && p2.x - 0.001 < p1.x) {
			this.lengthText.rotation(-90);
		} else {
			var slope = (p2.y - p1.y)/(p2.x - p1.x);
			var angle = Math.atan((p2.y - p1.y)/(p2.x - p1.x));
			this.lengthText.rotation(angle * 180 / Math.PI);
		}
		var distance = Math.pow(p1.x - p2.x, 2) + Math.pow((p1.y - p2.y), 2)
		distance = Math.pow(distance, 0.5);
		var truncated = Math.floor(distance * 100) / 100; // http://stackoverflow.com/questions/4912788/
		this.lengthText.text(truncated);
	};
	this.destroy = function() {
		this.line.destroy();
		this.centreMarker.destroy();
		this.lengthText.destroy();
	};
	layer.add(this.line);
	layer.add(this.centreMarker);
	layer.add(this.lengthText);
};